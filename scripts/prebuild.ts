import { join } from 'path'
import { remove } from 'fs-extra'

export const main = async () => {
	await remove(join(__dirname, '..', 'dist')).catch(ignoreNoEntry)
}

const ignoreNoEntry = (x: { code?: string }) =>
	'ENOENT' === x?.code || Promise.reject(x)

module.parent ||
	main().catch(x => {
		console.error(x)
		process.exit(1)
	})
