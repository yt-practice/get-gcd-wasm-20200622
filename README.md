# get-gcd

ユークリッドの互除法で最大公約数を求めるライブラリ  
（ Node.js 用 ）

```sh
yarn add gitlab:yt-practice/get-gcd-wasm-20200622#semver:v1.x
```

```ts
import { getGcd } from 'get-gcd-wasm-practice'
getGcd(12, 9)
```
