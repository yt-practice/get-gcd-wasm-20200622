#![no_std]
use wasm_bindgen::prelude::*;

#[wasm_bindgen(js_name = getGcd)]
pub fn get_gcd(a: i32, b: i32) -> i32 {
	let mut a1 = a;
	let mut b1 = b;
	if a < b {
		a1 = b;
		b1 = a;
	};
	while b1 != 0 {
		let remainder = a1 % b1;
		a1 = b1;
		b1 = remainder;
	}
	a1
}
